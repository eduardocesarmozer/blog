class PostsController < ApplicationController
    
    # Load the page to create a new Post
    def new
        @post = Post.new
    end
   
   # Creates a new Post 
    def create
        @post = Post.new(post_params)
        @post.user_id = current_user.id
        
        respond_to do |f|
            if (@post.save)
                f.html { redirect_to "", notice: "Post created!" }
            else
                f.html { redirect_to "", notice: "Error: Post Not Saved." }
            end
        end
    end
    
    private
    # Allows certain data to be passed via form
    def post_params
        params.require(:post).permit(:user_id, :content)
    end
    
end